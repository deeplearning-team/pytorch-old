#!/bin/sh -e
Version="1.3.0"
Dir="pytorch-${Version}"
Git="https://github.com/pytorch/pytorch"
Hash="de394b6"

Info () {
	echo "[38;5;42m *" $@ "[0;m"
}

RM () {
	echo "RM: [31;0m" $1 "[0;m"
	if test -r $1; then rm -rf $1; fi
}

test -d ${Dir} && \
	(Info Directory [${Dir}] already exists!; exit 1)

test -d ${Dir}.orig || ( \
	Info Cloning pytorch [v${Version}] ...;\
	git clone ${Git} --recursive -b v${Version} ${Dir}.orig --depth=100;\
	cd ${Dir}.orig; git checkout ${Hash}; \
	git submodule; \
	git submodule sync; \
	git submodule update --init --recursive;)

#test -d asmjit.tar.gz || (
#	Info Downloading asmjit ...;\
#	wget -c https://github.com/asmjit/asmjit/archive/673dcefaa048c5f5a2bf8b85daf8f7b9978d018a.tar.gz -O asmjit.tar.gz)

Info Copying pytorch [v${Version}] ...;
	cp -r ${Dir}.orig ${Dir};

Info Removing Nvidia Cruft [cub]...
	RM ${Dir}/third_party/cub

Info Removing Nvidia Cruft [onnx-tensorrt]...
	RM ${Dir}/third_party/onnx-tensorrt

Info Removing Nvidia Cruft [nccl]...
	RM ${Dir}/third_party/nccl

Info Removing IOS stuff ...
	RM ${Dir}/third_party/ios*

Info Removing Embedded Protobuf ...
	RM ${Dir}/third_party/protobuf

Info Removing Embedded Ideep ...
	RM ${Dir}/third_party/ideep

# Caffe2 OPs need the embedded eigen
#Info Removing Embedded Eigen ...
#	rm -rf ${Dir}/third_party/eigen

Info Removing Git Directories & Files ...
	for I in $(find ${Dir} -type d -name '.git'); do RM $I; done
	for I in $(find ${Dir} -type f -name '.gitmodules'); do RM $I; done
	for I in $(find ${Dir} -type f -name '.gitignore'); do RM $I; done

#Info Extracting ASMJIT ...
#	mkdir -p ${Dir}/third_party/fbgemm/third_party/asmjit
#	tar xf asmjit.tar.gz --strip-components=1 -C ${Dir}/third_party/fbgemm/third_party/asmjit

Info Creating Tarball ...
	tar cf - ${Dir} | xz -T0 > pytorch_${Version}+ds.orig.tar.xz
